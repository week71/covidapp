

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RiskWidget extends StatefulWidget {
  RiskWidget({Key? key}) : super(key: key);

  @override
  _RiskWidgetState createState() => _RiskWidgetState();
}

class _RiskWidgetState extends State<RiskWidget> {
  var riskQuestion = [
    'โรคทางเดินหายใจเรื้อรัง',
    'โรคหัวใจและหลอดเลือด',
    'โรคไตวายเรื้อรัง',
    'โรคหลอดเลือดสมอง',
    'โรคอ้วน',
    'โรคมะเร็ง',
    'โรคเบาหวาน',
  ];
  var riskQuestionValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Risk'),
      ),
      body: ListView(
        children: [
          CheckboxListTile(
              value: riskQuestionValues[0],
              title: Text(riskQuestion[0]),
              onChanged: (newValue) {
                setState(() {
                  riskQuestionValues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskQuestionValues[1],
              title: Text(riskQuestion[1]),
              onChanged: (newValue) {
                setState(() {
                  riskQuestionValues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskQuestionValues[2],
              title: Text(riskQuestion[2]),
              onChanged: (newValue) {
                setState(() {
                  riskQuestionValues[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskQuestionValues[3],
              title: Text(riskQuestion[3]),
              onChanged: (newValue) {
                setState(() {
                  riskQuestionValues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskQuestionValues[4],
              title: Text(riskQuestion[4]),
              onChanged: (newValue) {
                setState(() {
                  riskQuestionValues[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskQuestionValues[5],
              title: Text(riskQuestion[5]),
              onChanged: (newValue) {
                setState(() {
                  riskQuestionValues[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskQuestionValues[6],
              title: Text(riskQuestion[6]),
              onChanged: (newValue) {
                setState(() {
                  riskQuestionValues[6] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saveRiskQuestion();
                Navigator.pop(context);
              },
              child: const Text('Save'))
        ],
      ),
    );
  }
  @override
  void initState(){
    super.initState();
    _loadRiskQuestion();
  }
  
  Future<void> _loadRiskQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strRiskQuestionValue = prefs.getString('riskQuestion_values') ??
    '[false, false, false, false, false, false, false]';
    var arrStrRiskQuestionValues = 
    strRiskQuestionValue.substring(1,strRiskQuestionValue.length-1).split(',');
    setState(() {
      for(var i = 0;i<arrStrRiskQuestionValues.length;i++){
        riskQuestionValues[i] = (arrStrRiskQuestionValues[i].trim()=='true');
      }
    });

  }
  Future<void> _saveRiskQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('riskQuestion_values', riskQuestionValues.toString());
  }
}
